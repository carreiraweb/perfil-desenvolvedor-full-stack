# Git

Esse documento visa funcionar tanto como um resumo quanto como uma introdução à ferramenta CLI `git`.

## O que é?

`git` é uma ferramenta de **controle de versão** usada primariamente no desenvolvimento de software. Através de simples comandos em uma interface CLI ela permite salvar versões de um código, retornar a versões anteriores, criar e trabalhar com branches, sincronizar o projeto com servidores remotos, resolver conflitos entre colaboradoes e mais.

O `git` é uma ferramenta de código aberto amplamente reconhecida pela sua robustez, agilidade, usabilidade e ubiquidade.

## Inicializando um repositório

Um **repositório** representa um projeto sendo executado por um indivíduo ou uma equipe. Para **inicializar um repositório** dentro de um diretório execute o comando `git init`.

Caso você deseje ao invés disso **clonar um repositório** já existente use o comando `git clone` seguido da localização do diretório, seja na sua máquina, seja em um **repositório remoto**, fornecendo a URL para este último. O repositório local vai por padrão ficar apontando para a sua **origin**, para poder se manter sincronizado com ela. Para ver a sua origin use o comando `git remote`.

## Versões e o commit

Um código possui **versões**, criadas conforme ele vai evoluindo e passando por mudanças.

Quando você desejar criar uma nova versão você precisa, em primeiro lugar, **adicionar novos arquivos ao projeto**, caso eles existam. Para isso digite, da raíz do repositório, `git add` seguido do nome dos novos arquivos (`git add .` garante que todos os arquios no diretório serão adicionados). Ao fazer isso eles passam a ser **rastreados** pelo git.

Após isso dê um **commit** usando `git commit -m` seguido de uma mensagem. Essa mensagem deve expressar de maneira sucinta a mudança realizada. Quando desejar voltar para uma versão antiga do código você poderá usá-la para se localizar entre os diversos commits. 

É possível usar a flag `--amend` ao comando `git commit -m "mensagem"` para que as mudanças sejam incorporadas ao commit anterior, evitando assim poluir seu histórico de commits. 

O comando `git status` permite que verifique quais arquivos fazem ou não parte do projeto, além de outras informações. Caso você queira ou precise manter arquivos no diretório que não são parte do projeto adicione os seus nomes (junto do caminho relativo à raíz do projeto) a um arquivo chamado `.gitignore` localizado na raíz do projeto.

É possível reverter para uma versão mais antiga da branch ao **mudar a HEAD** do repositório. A HEAD é um ponteiro que aponta para a versão do commit que você está usando. Ao usar o comando `git reset HEAD~2 --soft` você vai dois commits "para o passado". `git reset HEAD~2 --hard` faz o mesmo, mas deletando os commits "do futuro".

Através do comando `git log` é possível ver o **histórico** de um repositório.

## Branches

Uma **branch** é um outro "galho" do seu código. Você pode querer bifurcar o seu código para manter uma versão de produção, uma de desenvolvimento, uma de teste, e assim vai. Uma representação do relacionamento entre possíveis branches pode ser vista abaixo:

![Branches](https://i.stack.imgur.com/F00b8.png)

A branch onde a gente inicialmente se encontra é conhecida como `master`. Para **conferir** em qual branch o seu código atual se encontra e outras branches presentes use o comando `git branch`.

Para **criar** uma nova branch use o comando `git branch nome_da_branch` (a flag `-D` permite **deletá-la**). Você pode mudar para uma outra branch usando o comando `git checkout nome_da_branch`.

Para **incorporar** as mudanças de uma branch em outra use o comando `git merge nome_da_branch`. Quando fazendo o merge é possível que haja **conflitos** entre os códigos. Isso ocorre quando outro desenvolvedor fez alterações em áreas do código que você editou nesse meio tempo. Quando o git acusar conflito ele marca no código atual os conflitos. Nesse caso resolvá-os e tente fazer o merge novamente.

## Push e Pull

Para mantermos o repositório local **sincronizado** com a origin precisamos user os comando `git push` e `git pull`, para atualizar do local para o remoto e vice-versa. Caso você queira especificar a origem e o branch a serem sincronizados você pode seguir ambos os comandos com essas informações, por exemplo, `git push origin master`.

Porém quando sua origin é um repositório em serviços como GitLab e o GitHub é necessário estar **autenticado** com uma conta que seja contribuidora do projeto em questão. Para isso use os comandos `git config user.name "NOME SOBRENOME"` e `git config user.email "my@email.com"`. Também é possível configurar para fazer essa autenticação por **SSH**.


## Tabela de comandos

Comando | Explicação | Comando | Explicação
--------|------------|---------|-----------
git init | inicia um novo repositório em um diretório | git clone | clona um repositório existente em um diretório
git add | adiciona arquivos ao projeto | git commit | cria uma nova versão do código
git status | mostra informações sobre o estado atual de um projeto | git branch | visualiza, cria e deleta branches
git checkout | muda de branch e descarta modificações de arquivos | git merge | incorpora mudanças de uma branch em outra
 git push | atualizar o repositório remoto com o código local | git pull | atualizar o código local com o repositório remoto
git config | permite configurar valores do sistema | git remote | permite visualizar sua origin e configurá-la
git clean | remove arquivos não-rastreados do diretório do projeto | git diff | mostra as mudanças que não foram commitadas em um arquivo
git reset | permite mudar a HEAD