# Linux

A meta desse arquivo é fazer tanto um resumo quanto uma introdução aos principais conceitos de Linux e Bash. Assume-se que o leitor já possui experiência com outros sistemas operacionais.

## Terminais e shells

O **terminal**, ou o emulador de terminal, fornece uma interface por linha de comando (ou **CLI** - _Command Line Interface_) para se interagir com a máquina. Por causa do seu poder, flexibilidade e capacidade de automatização recomenda-se enfaticamente o uso de CLIs para desenvolvedores ou usuários que desejam extrair mais do seu sistema.

O terminal é a ferramenta mais útil de um ambiente Linux. Na maioria dos ambientes modernos é possível encontrá-lo no menu de aplicações, ou como um atalho na área de trabalho. 

O **shell** é a ferramenta que processa os comandos em um terminal, e hoje o shell mais comum em máquinas Linux é o **Bash** (_Bourne-Again SHell_). Devido ao seu papel em um sistema, o shell define não só como os comandos serão processados, mas também a sua sintaxe. Assim nesse documento trabalharemos com uma sintaxe conhecida como Bash.

## Variáveis de ambiente

**Variáveis de ambiente**, assim como toda variável, guardam valores. Elas possuem esse nome pois são relativas a um ambiente shell, e geralmente possuem a sintaxe `$VARIAVEL_TODA_EM_MAIUSCULA`. Exemplos de variáveis de ambiente comuns são `$HOME`, `$USER` e `$SHELL`, cada uma armazenando uma informação importante para o sistema. 

Você pode ver o valor de uma variável usando o comando `echo`, que possui o papel de exibir um valor como **saída**. Por exemplo, digite `echo $USER` para ver o nome do **usuário** atualmente logado nessa **sessão do shell**. 

Você pode definir suas próprias variáveis de ambiente quando for necessário.

## Sintaxe básica do Bash

Comandos em Bash geralmente possuem a estrutura `nome_do_programa parametro1 paramentro2`, com qualquer número de parâmetros podendo ser aceito, dependendo do **programa** sendo executado.

Por exemplo, `echo $USER $HOME` retornará o nome do usuário logado no sistema e o seu **diretório _home_**. O programa `echo` aceita várias variáveis de ambiente como parâmetros. Ele também aceita **_strings_** como parâmetros. Tente rodar, por exemplo, `echo 'O diretório home de' $USER 'se localiza em' $HOME` — e note que este recebeu quatro parâmetros distintos.

Um programa também pode aceitar **_flags_**. Essas, geralmente e por convenção, são definidas por um ou dois traços na frente. Por exemplo, no comando `echo -e 'Foo\nBar'`, a _flag_ `-e` dita que a barra invertida antes de certas letras (`\n`, por exemplo) será tratada como um **caractere de escape**.

Você pode ler mais sobre a funcionalidade de um programa, incluindo os parâmetros e flags aceitos, através da sua **_manpage_**, que funciona como um manual do programa. Para isso digite `man nome_do_programa`. Programas também costumam usar a flag `-h`, de _help_, para fornecer **ajuda**.

## Diretórios e arquivos

**Sessões shell** possuem a propriedade de sempre se localizarem em um **diretório**. Geralmente você pode ver

1. O usuário no qual você está logado
1. O nome da máquina
1. O diretório atual

no próprio terminal, logo atrás do cursor. Um exemplo é `usuario@maquina: ~/diretorio/atual$`. O diretório atual também pode ser visualizado de forma completa através do programa `pwd` (_Print Working Directory_).

Talvez o diretório mais importante de um sistema Linux seja o _home_. Ao inicar uma sessão você geralmente se encontra nele. Ele é comumente abreviado para `~`. Para saber seu diretório home dê `echo ~` ou `echo $HOME`.

Para trocar de diretório use o programa `cd` (_Change Directory_), seguido do diretório desejado:

* `cd Documents` te leva para o diretório `Documents` dentro do diretório atual (**caminho relativo**).
* Se você não estiver mais no diretório _home_, dentro do qual o `Documents` se localiza, use `cd ~/Documents` para acessá-lo.
* Você também pode usar o **caminho absoluto**, iniciado por uma barra (`cd /home/usuario/Documents`). Note que somente uma barra `/` representa a **raíz** do seu sistema de arquivos. Assim, `cd /` para acessá-la.
* Agora que você está dentro do diretório `Documents` suponha que há o diretório `Downloads` também dentro da home e você quer acessá-lo. Ao invés de usar `cd /home/usuario/Downloads` você pode usar `..` para indicar **um diretório acima** (`cd ../Downloads`). Um ponto (`.`) representa o diretório atual.

Para listar os **arquivos** e diretórios dentro do diretório atual use o programa `ls` sem parâmetros. Também é possível passar diretórios como parâmetro para listá-los (`ls ~/Downloads`).

O programa `mkdir /caminho/nome_do_novo_diretorio` permite que se **crie novos diretórios**. `rm ~/caminho/nome_do_arquivo.ext` permite **apagar** o arquivo de **extensão** `.ext`. Porém para usar o programa `rm` com diretórios é necessária a _flag_ `-r`, que indica uma deleção **recursiva**. Para **criar um arquivo vazio** use o comando `touch nome_do_arquivo`. Todos esses comandos aceitam mais de um diretório ou arquivo como parâmetro.

O programa `cp` permite realizar **cópias** de arquivos e diretórios (este último usando a _flag_ `-r`). Por exemplo, `cp arquivo.mp4 /diretorio/arquivo.mp4` para fazer uma cópia do `arquivo.mp4` dentro de `/diretorio`. Já o programa `mv` de forma similar permite **mover** arquivos e diretórios.

## Plaintext, redirecionamento e piping

**_Plaintext_** nada mais é do que texto simples, usando um _enconding_ aberto como ASCII ou UTF-8. É comum usar a extensão `.txt` para denotar arquivos de _plaintext_.

Existem diversos programas para lidar com esses arquivos direto do terminal. Por exemplo, use `cat arquivo.txt` para simplesmente **exibir os conteúdos de um arquivo** na saída do terminal.

Porém para arquivos maiores pode se tornar imprático o uso do `cat`. Nesse caso use o programa `less` para inspecionar em tela inteira um arquivo _plaintext_.

Você pode escrever em um arquivo usando o próprio comando `echo`. Para isso use o símbolo de maior que (`>`) da seguinte forma `echo 'text' > nome_do_arquivo.txt`. Se o arquivo não existe ele será criado. Isso é conhecido como **redirecionamento de saída**.

* O símbolo `>` pega a saída do comando da esquerda e **sobrescreve** o arquivo à direita.
* Já o símbolo `>>` **concatena** a saída ao final do arquivo.
* O símbolo `<` permite usar o arquivo à direita como **entrada** do programa à esquerda. Por exemplo, `mail nome@provedor.com < arquivo.txt` permite mandar um e-mail com o conteúdo do `arquivo.txt`.
* De forma similar existe o símbolo _**pipe**_ (`|`) que permite usar a saída do programa à esquerda como entrada do programa à direita. `ls /diretorio | grep arquivo` pega a saída do programa `ls`, que lista o conteúdo de um diretório, e manda para o programa à esquerda, `grep`, que vai usar **expressões regulares** para procurar o padrão `arquivo` lá. 

Existem programas próprios para **edição de texto**, como `nano`, `vim` e `emacs`. Alguns deles possuem funções poderosas de edição e automatização, e por isso vale a pena aprendê-los. Para criar ou abrir um programa com o `vim` use simplesmente `vim nome_do_arquivo.txt`.

## Permissões e o programa "sudo"

Ambientes Linux, por questão de segurança, utilizam um sistema de **permissões** para realizar alterações no sistema. Essas permissões são baseadas em usuários e em **grupos** aos quais eles pertencem. Cada arquivo no sistema possui um conjunto de permissões definidas.

As permissões são divididas em três categorias: _Read_, _Write_ e _eXecute_ (**ler**, **escrever** e **executar**, respectivamente). A permissão _execute_ é checada antes de rodar **scripts** e **binários**.

Para um dado diretório use o comando `ls -l` para ver informações de permissões sobre os itens ali presentes. Para cada item serão mostrados três conjuntos de `rwx`, omitidos com um traço quando a permissão em questão não existe, sendo eles:

1. Permissões para o **dono** do item. O nome do dono é mostrado logo após os três `rwx`.
1. Permissões para membros do grupo ao qual o item pertence. O nome do grupo pode ser visto na frente do nome do dono.
1. Permissões para qualquer outro usuário.

Você pode **alterar permissões** com o programa `chmod`, desde que você possua a devida permissão, usando a seguinte lógica: Cada permissão é um bit; assim um conjunto `rwx` possui 3 bits, que podem ser representados por um número de 0 a 7. Dessa forma, use `chmod 700 nome_do_arquivo` para dar todas as permissões para o dono do arquivo, e nenhuma para qualquer outro usuário, por exemplo, ou `chmod 666` para todos poderem ler e escrever, mas nunca executar. `chmod +x` te dá especificamente a permissão de executar um arquivo, quando for necessário.

Através do programa `id` você pode ver o nome do usuário com o qual você está logado, além de informações sobre grupos aos quais ele pertence. 

Nos sistemas Linux existe um usuário especial conhecido como **_superuser_**, ou **root**. Arquivos sensíveis do sistema só podem ser alterados por ele. É recomendável tomar cuidado ao usá-lo e não executar nada como root sem necessidade. Para executar um comando como root digite `sudo` seguido do comando. Será pedida a senha do root, que foi definida durante a instalação do sistema. Por exemplo `sudo /diretorio/binario` roda o binário como root.

## Bash scripts

Toda a sintaxe vista aqui é válida não somente para comandos inseridos no terminal, mas também para scripts executados em um shell Bash. **Bash scripts**,como são conhecidos, são amplamente usados para automatização de tarefas e desenvolvimento de software.

Para criar um Bash script é só gerar um arquivo em _plaintext_ com a extensão `.sh` e com a primeira linha sendo `#!/bin/bash`, este podendo ser subtituído pelo caminho absoluto de qualquer shell presente no sistema. Para rodá-lo garanta que você possui a permissão _execute_ e simplesmente digite o caminho até ele no terminal.

A sintaxe Bash é poderosa e possui mecanismos diversos como variáveis, laços de repetição, operadores de comparação e até funções, fazendo dela Turing-completa.

Variáveis em Bash não possuem tipos. Porém dependendo do contexto é possível realizar operações aritiméticas sobre elas:

```Bash
#!/bin/bash

a=15
let "a+=1"
echo "a = $a"

b="string"
echo "b = $b"
```

É possível realizar um loop for da maneira clássica, estilo C, ou de uma forma mais moderna:

```Bash
# Iterando sobre uma lista. O * representa um coringa, ou seja, será retornando todos os arquvios em /etc que comecem com "rc.".

for i in /etc/rc.*; do
    echo $i
done

# "For" no estilo C.

for ((i = 0 ; i < 100 ; i++)); do
    echo $i
done
```

Condicionais e operadores de comparação possuem a seguinte forma:

```Bash
if [[ $a < 5 ]];  then
    echo "variavel a é menor 5"
elif [[ $a == $b ]]; then
    echo "a e b são iguais (ambas $a)"
else
    echo "não passou em nenhum dos testes"
fi
```

E funções podem ser definidas e chamadas assim:

```Bash
data_e_hora() {
    # executa o comando "date", que mostra data e hora
    echo $(date)
}

data_e_hora
```

Assim como qualquer linguagem de programação a sintaxe Bash possui inúmeros detalhes e peculiariedades, o suficiente para colocar uma análise mais profunda fora do escopo desse documento. Porém, para fazer uso efetivo dela vale um estudo maior por parte do usuário.

# Tabela de comandos

Comandos em negrito não foram apresentados no texto.

## Diretórios e arquivos

Comando | Descrição | Comando | Descrição
--------|-----------|---------|----------
pwd | diretório absoluto atual | cd | mudar de diretório
ls | listar itens em um diretório | mkdir | criar diretório
rm | remover item | touch | criar arquivo ou atualiza acesso de um exisistente
cp | copiar item | mv | mover item
**find**, **locate** | localizar arquivos no sistema | **which** | mostra qual binário ou script um dado _alias_ executa
**file** | mostra informações de um arquivo | **tar, zip, unrar** | trabalha com arquivos comprimidos

## Texto

Comando | Descrição | Comando | Descrição
--------|-----------|---------|----------
cat | visualizar texto na saída | less | visualizador de texto
**sort** | reordena as linhas | grep | realizar busca em texto com regex
nano, vim, emacs | editores de texto | **uniq** | filtra somente as linhas únicas
**tee** | salva entrada em arquivo e exibe na tela | **tail** | exibe final do texto

## Permissões

Comando | Descrição | Comando | Descrição
--------|-----------|---------|----------
chmod | mudar permissões | sudo | rodar como root
**chown** | mudar dono de item | id | lista informações de usuário
**su** | permite rodar comandos como outro usuário | 

## Rede

Comando | Descrição | Comando | Descrição
--------|-----------|---------|----------
**ssh** | logar em um sistema remoto | **wget** | baixa arquivo em servidor remoto
**ifconfig** | informações sobre as interfaces de rede | **ping** | tenta se conectar a um sevidor remoto
**traceroute** | mostra rota até máquina remota | mail | mandar e-mail
**telnet** | se conecta a uma maquina via telnet | **curl** | faz requisições para diversos protocolos

## Sistema

Comando | Descrição | Comando | Descrição
--------|-----------|---------|----------
echo | printa na tela uma informação | man | manual de um programa
**htop** | ver processos rodando no sistema | **apt, pacman, rpm** | gerenciadores de pacotes (software)
**kill, xkill** | mata processos | **uname** | mostra informações sobre o sistema
**clear** | limpa a tela | **cron** | permite agendar eventos (_crobjob_)
date | mostra informações de data e hora | **printenv** | lista variáveis de ambiente
**exit** | fecha uma sessão shell | **du** | mostra informações sobre uso de disco
**exec** | substitui o shell por outra aplicação

## Símbolos
Comando | Descrição | Comando | Descrição
--------|-----------|---------|----------
-a | _flag_ | $VAR | variável
~ | _home_ | .. | um diretório acima
. | diretório atual | > | sobrescreve saída em arquivo
\>\> | concatena saída em arquivo | < | usa arquivo como entrada
\| | usa saída de um programa como entrada em outro (piping) | * | coringa - substitui qualquer substring